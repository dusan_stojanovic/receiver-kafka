package main

import (
	"fmt"
	"log"
	"receiver-kafka/kafkaconsumer"
)

func main() {
	log.Println("Starting Receiver Service")
	consumer := kafkaconsumer.InitKafkaConsumer()
	err := consumer.ReceiveMessage("messages")
	if err != nil {
		fmt.Println(err)
	}
}
