package kafkaconsumer

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type KafkaConsumer struct {
	consumer *kafka.Consumer
}

func InitKafkaConsumer() *KafkaConsumer {
	conf := &kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"group.id":          "kafka-consumer",
	}

	consumer, err := kafka.NewConsumer(conf)
	if err != nil {
		log.Fatalf("Kafka consumer error: %v", err)
	}

	return &KafkaConsumer{
		consumer: consumer,
	}
}

func (kc *KafkaConsumer) ReceiveMessage(topic string) error {
	err := kc.consumer.SubscribeTopics([]string{topic}, nil)
	if err != nil {
		return err
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	for {
		select {
		case <-interrupt:
			// Gracefully shut down the consumer
			kc.consumer.Close()
			return nil
		default:
			msg, err := kc.consumer.ReadMessage(time.Second)
			if err != nil {
				continue
			}

			fmt.Println(string(msg.Value))
		}
	}
}
