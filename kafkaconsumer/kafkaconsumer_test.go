package kafkaconsumer

import (
	"fmt"
	"log"
	"syscall"
	"testing"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/stretchr/testify/assert"
)

func TestReceiveMessage(t *testing.T) {
	kafkaConsumer := initConsumer()
	topic := "test-events"
	sendMessage(topic)

	go func() {
		time.Sleep(5 * time.Second)
		syscall.Kill(syscall.Getpid(), syscall.SIGINT)
	}()

	err := kafkaConsumer.ReceiveMessage(topic)
	assert.Equal(t, nil, err)
}

func initConsumer() *KafkaConsumer {
	conf := &kafka.ConfigMap{
		"bootstrap.servers": "localhost",
		"group.id":          "kafka-consumer-test",
	}

	consumer, _ := kafka.NewConsumer(conf)

	// Return consumer
	return &KafkaConsumer{
		consumer: consumer,
	}
}

func sendMessage(topic string) {
	// Init producer
	confProducer := &kafka.ConfigMap{
		"bootstrap.servers": "localhost",
	}

	producer, err := kafka.NewProducer(confProducer)
	if err != nil {
		log.Fatalf("Kafka producer error: %v", err)
	}

	// Send test message
	topicPartition := kafka.TopicPartition{
		Topic:     &topic,
		Partition: kafka.PartitionAny,
	}

	message := &kafka.Message{
		TopicPartition: topicPartition,
		Key:            []byte("Test ID"),
		Value:          []byte("Test message"),
	}

	producer.Produce(message, nil)
	fmt.Println("Message sent")
}
